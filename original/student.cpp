#include <iostream>
#include "C:\Users\User\Documents\Magshimim\2016-2017\CPP\Assignment2\2\Student.h"

using std::string;
string _name;
Course** _courses;
int _crsCount;

void init(string name, Course** courses, int crsCount)
{
	_name = name;
	_courses = courses;
	_crsCount = crsCount;
}
string getName()
{
	return _name;
}
void setName(string name)
{
	_name = name;
}
int getCrsCount()
{
	return _crsCount;
}
Course** getCourses()
{
	return _courses;
}
double getAvg()
{
	double avg = 0;
	for (int i = 0; i < _crsCount; i++)
	{
		avg += _courses[i]->getFinalGrade();
	}
	return avg / (double)_crsCount;
}
